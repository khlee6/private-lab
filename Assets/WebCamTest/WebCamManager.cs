using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebCamManager : MonoBehaviour
{
    [SerializeField] private WebCamController _controller;
    [SerializeField] private Button _button;
    [SerializeField] private Image _image;

    private Texture2D _capturedTexture = null;

    private void Start()
    {
        _button.onClick.AddListener(delegate
        {
            if (_image.sprite != null)
                Destroy(_image.sprite);
            if (_capturedTexture != null)
                Destroy(_capturedTexture);

            _capturedTexture = _controller.Capture();
            Sprite sprite = Sprite.Create(_capturedTexture, new Rect(0, 0, _capturedTexture.width, _capturedTexture.height), new Vector2(0.5f, 0.5f));

            _image.sprite = sprite;
            ((RectTransform)_image.transform).sizeDelta = new Vector2(_capturedTexture.width, _capturedTexture.height);
        });

        _controller.Initialize((WebCamController.Error error) =>
        {
            if (error == WebCamController.Error.Success)
                _controller.StartWebCam();
        });
    }

    private void OnDestroy()
    {
        _controller.StopWebCam();
    }
}
