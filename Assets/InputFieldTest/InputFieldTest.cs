using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldTest : MonoBehaviour
{
    [SerializeField] private InputField _inputField;
    [SerializeField] private Button _button;

    private void Awake()
    {
        _inputField.onEndEdit.AddListener(delegate { Debug.Log("onEndEdit"); });
        _button.onClick.AddListener(delegate
        {
            TouchScreenKeyboard keyboard = TouchScreenKeyboard.Open("");
            keyboard.text = "";
        });
    }
}
