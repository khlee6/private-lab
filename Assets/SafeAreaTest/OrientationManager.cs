using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrientationManager : MonoBehaviour
{
	private static OrientationManager mInstance = null;

	public static OrientationManager Instance
    {
		get
        {
			if (mInstance == null)
				mInstance = FindObjectOfType<OrientationManager>();

			return mInstance;
		}
    }

	public delegate void OrientationDelegate(ScreenOrientation orientation);
	public OrientationDelegate OrientationEvent;

	public ScreenOrientation CurrentOrientation;
	private ScreenOrientation PrevOrientation;

	private void Awake()
	{
#if !UNITY_EDITOR
		SetOrientationByDevice();
		PrevOrientation = CurrentOrientation;
#else
		CurrentOrientation = ScreenOrientation.Portrait;
#endif
	}

	private void Start()
	{
		OrientationEvent?.Invoke(CurrentOrientation);
	}

	private void LateUpdate()
	{
#if !UNITY_EDITOR
		SetOrientationByDevice();
#else
		SetOrientationByEditor();
#endif
		if (CurrentOrientation != PrevOrientation)
		{
			OrientationEvent?.Invoke(CurrentOrientation);
			PrevOrientation = CurrentOrientation;
		}
	}

	public void SetOrientationByDevice()
	{
		PrevOrientation = CurrentOrientation;

		ScreenOrientation orientation = Screen.orientation;

		if (orientation == ScreenOrientation.LandscapeLeft ||
			orientation == ScreenOrientation.LandscapeRight)
		{
			CurrentOrientation = ScreenOrientation.Landscape;
		}
		else
		{
			CurrentOrientation = ScreenOrientation.Portrait;
		}
	}

	public void SetOrientationByEditor()
	{
		PrevOrientation = CurrentOrientation;

		if (Screen.width > Screen.height)
		{
			CurrentOrientation = ScreenOrientation.Landscape;
		}
		else
		{
			CurrentOrientation = ScreenOrientation.Portrait;
		}
	}
}
