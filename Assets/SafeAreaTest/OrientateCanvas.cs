using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum State { Portrait, LandScape };

public class OrientateCanvas : MonoBehaviour
{
	public void AddEvent()
	{
		if (OrientationManager.Instance != null)
		{
			OrientationManager.Instance.OrientationEvent += ChangeByOrientation;
		}
	}

	public void DeleteEvent()
	{
		if (OrientationManager.Instance != null)
		{
			OrientationManager.Instance.OrientationEvent -= ChangeByOrientation;
		}
	}

	public static OrientateCanvas instance;

	public State state = State.Portrait;

	private CanvasScaler _canvasScaler;

	[SerializeField] private float _refResolutionX = 720f;
	[SerializeField] private float _refResolutionY = 1280f;

	private void Awake()
	{
		instance = this;

		_canvasScaler = GetComponent<CanvasScaler>();
		AddEvent();
	}

	public void ChangeByOrientation(ScreenOrientation orientation)
	{
		if (_canvasScaler == null)
		{
			Debug.LogError("Canvas scaler not set");
			return;
		}

		if (orientation == ScreenOrientation.Portrait)
		{
			state = State.Portrait;
			_canvasScaler.referenceResolution = new Vector2(_refResolutionX, _refResolutionY);

			switch (_canvasScaler.screenMatchMode)
			{
				case CanvasScaler.ScreenMatchMode.MatchWidthOrHeight:
					_canvasScaler.matchWidthOrHeight = 0.0f;
					break;
			}
		}
		else
		{
			state = State.LandScape;
			_canvasScaler.referenceResolution = new Vector2(_refResolutionY, _refResolutionX);


			switch (_canvasScaler.screenMatchMode)
			{
				case CanvasScaler.ScreenMatchMode.MatchWidthOrHeight:
					_canvasScaler.matchWidthOrHeight = 1.0f;
					break;
			}
		}
	}
}
