using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SafeAreaTest : MonoBehaviour
{
    [Header("UI")]
    [SerializeField] private RectTransform _refTr;
    [SerializeField] private RectTransform _targetTr;

    [Header("Debug")]
    [SerializeField] private Text _safeAreaValueText;

    private Rect _currentRefRect;
    private Rect _currentSafeArea;

    private void OnEnable()
    {
        UpdateSafeArea();
    }

    private void Update()
    {
        if (!_refTr.rect.Equals(_currentRefRect) || !Screen.safeArea.Equals(_currentSafeArea))
            UpdateSafeArea();

        _safeAreaValueText.text =
            "x = " + Screen.safeArea.x.ToString() + "\n" +
            "y = " + Screen.safeArea.y.ToString() + "\n" +
            "width = " + Screen.safeArea.width.ToString() + "\n" +
            "height = " + Screen.safeArea.height.ToString();
    }

    private void UpdateSafeArea()
    {
        _currentRefRect = _refTr.rect;
        _currentSafeArea = Screen.safeArea;

        Vector2 offsetMin = Vector2.zero;
        Vector2 offsetMax = Vector2.zero;
        Vector2 ratio = new Vector2(_currentRefRect.width / Screen.width, _currentRefRect.height / Screen.height);

        /* Left */
        offsetMin.x = _currentSafeArea.x * ratio.x;
        /* Right */
        offsetMax.x = (Screen.width - _currentSafeArea.x - _currentSafeArea.width) * ratio.x;
        /* Top */
        offsetMax.y = (Screen.height - _currentSafeArea.y - _currentSafeArea.height) * ratio.y;
        /* Bottom */
        offsetMin.y = _currentSafeArea.y * ratio.y;

        _targetTr.offsetMin = offsetMin;
        _targetTr.offsetMax = -offsetMax;
    }
}
