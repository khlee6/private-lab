using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CoroutineTest : MonoBehaviour
{
    private void Start()
    {
        Coroutine coroutine = null;
        coroutine = StartCoroutine(TestFunc(delegate
        {
            Debug.Log("callback");
            StopCoroutine(coroutine);
        }));
        Debug.Log("Start finished");
    }

    private IEnumerator TestCoroutine()
    {
        yield return SendWeb(delegate
        {
            Debug.Log("Hi");
        });

        Debug.Log("Test");
    }

    private IEnumerator TestFunc(System.Action callback)
    {
        yield return null;
        Debug.Log("TestFunc called");
        callback?.Invoke();
        Debug.Log("TestFunc call TestFunc2");
        yield return TestFunc2();
        Debug.Log("TestFunc finished");
    }

    private IEnumerator TestFunc2()
    {
        Debug.Log("TestFunc2 called");
        yield return new WaitForSeconds(3.0f);
        Debug.Log("TestFunc2 finished");
    }

    private IEnumerator SendWeb(System.Action callback)
    {
        /* setup URL */
        string url = MakeURL("API", "/pudding/getInnerSentence_v2") +
            "?key=" + "community_recommend" +
            "&versionCode=" + "1" +
            "&languageCode=" + "ko-KR" +
            "&serviceCode=" + "naver" +
            "&voiceCode=" + "nara";

        /* setup request */
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

        /* wait some time.. */
        yield return new WaitForSeconds(3.0f);

        /* process response */
        Debug.Log(request.downloadHandler.text);

        /* wait some time.. */
        yield return new WaitForSeconds(3.0f);
        callback?.Invoke();
    }

    public static string MakeURL(string category, string api)
    {
        return GetServerURL() + category + "/" + GetCustomerCode() + "/Avadin" + api;
    }

    public static string GetServerURL()
    {
        return "https://channel.dasomi.ai/";
    }

    public static string GetCustomerCode()
    {
        return "solitary";
    }
}
