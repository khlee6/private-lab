using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class AsyncTest : MonoBehaviour
{
    private void Start()
    {
        //WaitAndGoAsync();
        Task.Run(() => FirstTestAsync());
        Debug.Log(Thread.CurrentThread.ManagedThreadId.ToString() + " Task started.");
    }

    private async Task FirstTestAsync()
    {
        Debug.Log(Thread.CurrentThread.ManagedThreadId.ToString() + " In Task a.");
        await Task.Run(() =>
        {
            Debug.Log(Thread.CurrentThread.ManagedThreadId.ToString() + " In Task b.");
            for (int i = 0; i < 1000; i++)
                print(i);
        });

        await Task.Delay(3000);

        await SecondTestAsync();
    }

    private async Task SecondTestAsync()
    {
        await Task.Delay(5000);

        for (int i = 0; i < 1000; i++)
            print(i);
    }

    private async void WaitAndGoAsync()
    {
        await Task.Delay(1000);
        Debug.Log(Thread.CurrentThread.ManagedThreadId.ToString() + " Task completed.");
    }
}
