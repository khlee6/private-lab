using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorTest : MonoBehaviour
{
    private void Update()
    {
        Debug.Log(Input.acceleration.x.ToString() + ", " + Input.acceleration.y.ToString() + ", " + Input.acceleration.z.ToString());
        Debug.Log(Input.gyro.rotationRate.x.ToString() + ", " + Input.gyro.rotationRate.y.ToString() + ", " + Input.gyro.rotationRate.z.ToString());
    }
}
