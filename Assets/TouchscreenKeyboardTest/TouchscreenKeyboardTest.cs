using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInputFieldPlugin;

public class TouchscreenKeyboardTest : MonoBehaviour
{
    private void Awake()
    {
        StartCoroutine(Test());

        NativeKeyboardManager.AddKeyboardHeightChangedListener((int height) =>
        {
            Debug.Log("height = " + height.ToString());
        });
    }

    private IEnumerator Test()
    {
        while (true)
        {
            Debug.Log("value = " + TouchScreenKeyboard.area.height.ToString());
            yield return new WaitForSeconds(1.0f);
        }
    }
}
