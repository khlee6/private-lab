using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TweeningTest : MonoBehaviour
{
    private Tweener mTweener = null;

    [SerializeField][Range(0.0f, 5.0f)]
    private float position = 0.0f;

    private void Start()
    {
        mTweener = transform.DOLocalMove(new Vector3(800.0f, 0.0f), 5.0f).SetEase(Ease.InOutBack).Pause().OnComplete(delegate
        {
            Debug.Log("completed");
        });
    }

    private void OnValidate()
    {
        if (mTweener != null)
            mTweener.Goto(position);
    }
}
