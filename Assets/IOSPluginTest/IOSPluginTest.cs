using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class IOSPluginTest : MonoBehaviour
{
    [SerializeField] private Text debugText;

#if !UNITY_EDITOR && UNITY_IOS
    [DllImport("__Internal")]
    private static extern float FooPluginFunction();
#endif

    private void Start()
    {
        debugText.text = Foo().ToString();
    }

    private float Foo()
    {
#if UNITY_EDITOR
        return 1.0f;
#elif UNITY_IOS
        return FooPluginFunction();
#else
        return 3.0f;
#endif
    }
}
