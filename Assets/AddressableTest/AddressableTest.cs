using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AddressableTest : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Button _button;
    [SerializeField] private Text _text;

    private readonly static string KEY = "Assets/AddressableTest/img_avadin.png";

    private void Awake()
    {
        _button.onClick.AddListener(delegate
        {
            Addressables.LoadAssetAsync<Sprite>(KEY).Completed += (AsyncOperationHandle<Sprite> obj) =>
            {
                _image.sprite = obj.Result;
                _text.text = obj.Status.ToString();
            };
        });
    }
}
